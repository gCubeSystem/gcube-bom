This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gCube Bom

## [v4.0.1-SNAPSHOT]

- Upgraded document-store-lib range to [4.0.0-SNAPSHOT,5.0.0-SNAPSHOT)
- Added d4science-iam-client range [1.0.0-SNAPSHOT, 2.0.0)

## [v4.0.0]

 - Moved to jakarta
 - Moved to java11 


## [v3.0.0] - 2023-08-03

- Moved to smartgears 4.0.0 
- Keycloak client added
- Enhanced ranges of common libraries
- Enhanced ranges of gx-rest libraries
- Enhanced ranges of storage-hub libraries
- Enhanced ranges of authorization libraries
- Added new libraries for authorization
- Replaced common-utility-sg3 with common-utility-sg4
- Enhanced information-system-model version range
- Enhanced gcube-model version version range
- Enhanced resource-registry-api lower bound of range
- Enhanced resource-registry-client lower bound of range
- Enhanced resource-registry-publisher lower bound of range
- Enhanced resource-registry-context-client lower bound of range
- Enhanced resource-registry-query-template-client lower bound of range
- Enhanced resource-registry-schema-client lower bound of range


## [v2.2.0] - 2023-03-09

- Enhanced information-system-model version range
- Enhanced gcube-model version lower bound of range
- Enhanced resource-registry-api lower bound of range
- Enhanced resource-registry-client lower bound of range
- Enhanced resource-registry-publisher lower bound of range
- Added common-utility-sg3


## [v2.1.0] - 2022-10-27

- Enhanced gcube-model version range
- Enhanced information-system-model version range
- Added json-simple library version 1.1.1 [#21692]
- Added storagehub dependencies in range 2,3 [#22730] [#22872]
- Removed authorization-utils
- Enhanced logback-classic version to 1.2.4



## [v2.0.2] - 2022-03-08

- Added storagehub dependecies in range 1,2 [#22925]  
- Added authorization-utils [#22550][#22871]


## [v2.0.1] - 2021-02-24 

- Updated resource registry clients versions

## [v2.0.0] - 2020-11-11 

- Switched JSON management to gcube-jackson [#19283]


## [v1.5.0] - 2020-03-30

- Removed -SNAPSHOT from dependencies lower bound of ranges


## [v1.4.0] - 2019-12-19

- Removed -SNAPSHOT from dependencies lower bound of ranges


## [v1.3.0] - 2019-05-27

- Added gxREST Dependency
- Added gxJRS Dependency
- Imported jersey-bom
- Added javax.ws.rs-api compatible with defined jersey version


## [v1.2.1] - 2019-02-26

- Upgraded slf4j-api from 1.7.5 to 1.7.25
- Added gxHTTP Dependency


## [v1.2.0] - 2018-11-20

- Updated resource-registry related clients and libraries [#11941]


## [v1.1.0] - 2017-11-29

- Added Jackson libraries [#9835]


## [v1.0.2] - 2017-06-06

- Changed version of accounting-lib and document-store-lib


## [v1.0.1] - 2016-12-15

- Removed wrong scope on dependency declaration


## [v1.0.0] - 2016-11-07

- First Release

